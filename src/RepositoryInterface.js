'use strict';

const RepositoryImplementationError = require('./Errors/RepositoryImplementationError');

/**
 * @class DBRepositoryInterface
 * @type DBRepositoryInterface
 * @interface
 */
class DBRepositoryInterface {

    /**
     *
     * @param {DBRepositoryLimitInterface} limit
     * @return {DBRepositoryInterface}
     * @abstract
     */
    // eslint-disable-next-line no-unused-vars
    setLimit(limit) {
        throw new RepositoryImplementationError(this, 'setLimit');
    }

    /**
     *
     * @param {[DBRepositoryOrderInterface]} orders
     * @return {DBRepositoryInterface}
     * @abstract
     */
    // eslint-disable-next-line no-unused-vars
    setOrders(orders) {
        throw new RepositoryImplementationError(this, 'setOrders');
    }

    /**
     * @return {Promise.<[EMEntity]>}
     * @abstract
     */
    // eslint-disable-next-line no-unused-vars
    find(fields = []) {
        throw new RepositoryImplementationError(this, 'find');
    }

    /**
     *
     * @param filter
     * @return {DBRepositoryInterface}
     * @abstract
     */
    // eslint-disable-next-line no-unused-vars
    addFilter(filter) {
        throw new RepositoryImplementationError(this, 'setFilter');
    }

    /**
     * @return {DBRepositoryInterface}
     * @abstract
     */
    cleanFilters() {
        throw new RepositoryImplementationError(this, 'cleanFilters');
    }
}

module.exports = DBRepositoryInterface;
