'use strict';

const RepositoryImplementationError = require('../Errors/RepositoryImplementationError');

/**
 * @class DBRepositoryLimitInterface
 * @type {DBRepositoryLimitInterface}
 * @abstract
 */
class DBRepositoryLimitInterface {
    /**
     * @return {number}
     * @abstract
     */
    getOffset() {
        throw new RepositoryImplementationError(this, 'getOffset');
    };

    /**
     * @return {number}
     * @abstract
     */
    getLimit() {
        throw new RepositoryImplementationError(this, 'getLimit');
    };

}

module.exports = DBRepositoryLimitInterface;
