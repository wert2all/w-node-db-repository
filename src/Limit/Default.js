'use strict';

const DBRepositoryLimitInterface = require('./Interface');

/**
 * @class DBRepositoryLimitDefault
 * @type DBRepositoryLimitInterface
 */
class DBRepositoryLimitDefault extends DBRepositoryLimitInterface {
    /**
     *
     * @param {number} current
     * @param {number} limit
     */
    constructor(current, limit) {
        super();
        /**
         *
         * @type {number}
         * @private
         */
        this._current = (current <= 0) ? 1 : current;
        /**
         *
         * @type {number}
         * @private
         */
        this._limit = limit;
    }

    /**
     * @return {number}
     */
    getOffset() {
        return (this._current - 1) * this._limit;
    };

    /**
     * @return {number}
     */
    getLimit() {
        return this._limit;
    };

}

module.exports = DBRepositoryLimitDefault;
