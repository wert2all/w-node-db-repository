'use strict';

const ThrowableImplementationError = require('w-node-implementation-error');

/**
 * @class DBRepositoryImplementationError
 * @type ThrowableImplementationError
 */
class RepositoryImplementationError extends ThrowableImplementationError {

}

module.exports = RepositoryImplementationError;
