'use strict';
/** @type {DBRepositoryInterface} */
const DBRepositoryInterface = require('./RepositoryInterface');
/** @type {DBRepositoryLimitDefault} */
const DBRepositoryLimitDefault = require('./Limit/Default');

/**
 * @class DBRepository
 * @type {DBRepositoryInterface}
 */
class DBRepository extends DBRepositoryInterface {
    /**
     *
     * @param {DBModelInterface} model
     * @param {[DBRepositoryFilterInterface]} filters
     * @param {DBRepositoryLimitInterface} limit
     * @param {[DBRepositoryOrderInterface]} orders
     */
    constructor(model, filters = [], limit = null, orders = []) {
        super();
        /**
         *
         * @type {DBModelInterface}
         * @private
         */
        this._model = model;
        /**
         * @type {DBRepositoryLimitInterface}
         * @private
         */
        this._limit = limit !== null ? limit : new DBRepositoryLimitDefault(1, 10);
        /**
         * @type {[DBRepositoryOrderInterface]}
         * @private
         */
        this._orders = orders;
        /**
         *
         * @type {DBRepositoryFilterInterface[]}
         * @private
         */
        this._filters = filters;
    }

    /**
     *
     * @param {DBRepositoryLimitInterface} limit
     * @return {DBRepositoryInterface}
     */
    setLimit(limit) {
        this._limit = limit;
        return this;
    }

    /**
     *
     * @param {[DBRepositoryOrderInterface]} orders
     * @return {DBRepositoryInterface}
     */
    setOrders(orders) {
        this._orders = orders;
        return this;
    }

    /**
     *
     * @param {DBRepositoryFilterInterface} filter
     * @return {DBRepositoryInterface}
     */
    addFilter(filter) {
        this._filters.push(filter);
        return this;
    }

    /**
     * @return {DBRepositoryInterface}
     */
    cleanFilters() {
        this._filters = [];
        return this;
    }

    /**
     *
     * @param {[]} fields
     * @return {Promise<[EMEntity]>}
     */
    find(fields = []) {
        /**
         *
         * @type {DBModelQueryInterface}
         */
        const query = this._model
            .getModelQuery()
            .setFields(fields)
            .setLimit(this._limit.getOffset(), this._limit.getLimit());

        if (this._model.getModelDefinition().getRelation() !== null) {
            query.addRelation(this._model.getModelDefinition().getRelation());
        }

        this._orders
            .map(order => order.getOrder())
            .forEach(order => query.addOrder(order[0], order[1]));

        this._filters
            .forEach(filter =>
                query.addFilter(
                    filter.getField(),
                    filter.getExpression()
                )
            );
        return this._model
            .fetch(query);
    }

}

module.exports = DBRepository;
