'use strict';

const DBRepositoryOrderInterface = require('./Interface');

/**
 * @class DBRepositoryOrderDefault
 * @type DBRepositoryOrderInterface
 */
class DBRepositoryOrderDefault extends DBRepositoryOrderInterface {
    /**
     *
     * @param {string} field
     * @param {string} orderType
     */
    constructor(field, orderType = 'desc') {
        super();
        this._field = field;
        this._type = (orderType === 'desc') ? 'desc' : 'asc';
    }

    /**
     *
     * @return {*}
     * @abstract
     */
    getOrder() {
        return [this._field, this._type];
    }

}

module.exports = DBRepositoryOrderDefault;
