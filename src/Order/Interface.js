'use strict';

const RepositoryImplementationError =
    require('./../Errors/RepositoryImplementationError');

/**
 * @class DBRepositoryOrderInterface
 * @type {DBRepositoryOrderInterface}
 */
class DBRepositoryOrderInterface {
    /**
     *
     * @return {*}
     * @abstract
     */
    getOrder() {
        throw new RepositoryImplementationError(this, 'getOrder');
    }

}

module.exports = DBRepositoryOrderInterface;
