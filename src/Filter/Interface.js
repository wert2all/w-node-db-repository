'use strict';

/** @type {ThrowableImplementationError} */
const RepositoryImplementationError =
    require('./../Errors/RepositoryImplementationError');

/**
 * @class DBRepositoryFilterInterface
 * @type {DBRepositoryFilterInterface}
 * @interface
 */
class DBRepositoryFilterInterface {

    /**
     * @return {string}
     */
    getField() {
        throw  new RepositoryImplementationError(this, 'getField');
    }

    /**
     * @return {{}|*}
     */
    getExpression() {
        throw  new RepositoryImplementationError(this, 'getValue');
    }
}

module.exports = DBRepositoryFilterInterface;
