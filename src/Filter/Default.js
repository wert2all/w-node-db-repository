'use strict';

const DBRepositoryFilterInterface = require('./Interface');

/**
 * @class DBRepositoryFilterDefault
 * @type {DBRepositoryFilterInterface}
 */
class DBRepositoryFilterDefault extends DBRepositoryFilterInterface {

    /**
     *
     * @param {string} field
     * @param {*} sign
     * @param {*} value
     */
    constructor(field, sign, value) {
        super();
        /**
         *
         * @type {string}
         * @private
         */
        this._field = field;
        /**
         *
         * @type {*}
         * @private
         */
        this._sign = sign;
        /**
         *
         * @type {*}
         * @private
         */
        this._value = value;
    }

    /**
     * @return {string}
     */
    getField() {
        return this._field;
    }

    /**
     * @return {{}|*}
     */
    getExpression() {
        return {
            [this._sign]: this._value
        };
    }
}

module.exports = DBRepositoryFilterDefault;
